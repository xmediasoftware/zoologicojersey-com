<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/zoologico_jersey_template.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Zoológico Valle de Guadalupe, B.C. Jersey - Bienvenido a el parque del Niño</title>
<meta name="description" content="Bienvenidos a el parque zoológico del niño valle de guadalupe en Ensenada, donde la diversión es familiar" />
<meta name="keywords" content="parque jersey valle de guadalupe, parque del niño valle de guadalupe, zoologico jersey, parque jersey" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<style type="text/css">
.style281 {	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: small;
}
.style29 {	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: small;
	color: #993333;
}
.style30 {	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: small;
}
.style31 {	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #FFFFFF;
}
</style>
<!-- InstanceEndEditable -->
<script type="text/javascript">

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<style type="text/css">
<!--
a:link {
	color: #FF9900;
	text-decoration: none;
}
a:visited {
	color: #FF9900;
	text-decoration: none;
}
a:hover {
	color: #FF0000;
	text-decoration: none;
}
a:active {
	text-decoration: none;
}
.style1 {
	color: #FF9900;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: xx-small;
	font-weight: bold;
}
.style4 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: x-small;
}
body {
	background-image: url(imagenes/images/fondoBig.jpg);
	background-repeat: repeat;
	background-color: #000000;
}
.style28 {
	color: #FF9900;
	font-size: large;
	font-family: Geneva, Arial, Helvetica, sans-serif;
}
.style11 {font-size: xx-small;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #666666;
}
.style35 {color: #FF9900; font-size: medium; font-family: Geneva, Arial, Helvetica, sans-serif; }
-->
</style>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-9640258-9']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onload="MM_preloadImages('imagenes/Head/head-02_03.jpg','imagenes/Head/head-02_04.jpg','imagenes/Head/head-02_05.jpg','imagenes/Head/head-02_06.jpg','imagenes/Head/head-02_07.jpg','imagenes/Head/head-02_08.jpg')">
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <th width="800" align="center" valign="top" scope="col"><table width="799" height="205" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
      <tr>
        <td rowspan="3"><a href="espanol.php"><img src="imagenes/Head/head-01_01.jpg" alt="Logotipo Zoologico Jersey" width="302" height="205" border="0" /></a></td>
        <td colspan="7"><img src="imagenes/Head/head-02_02.jpg" alt="Ven estas vacaciones, ven y conocenos y diviertete, ven y disfruta cono nunca!" width="497" height="133" /></td>
      </tr>
      <tr>
        <td><a href="espanol.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image25','','imagenes/Head/head-02_03.jpg',1)"><img src="imagenes/Head/head-01_03.jpg" alt="Ir a Página Principal" name="Image25" width="79" height="43" border="0" id="Image25" /></a></td>
        <td><a href="htmls/admision-al-parque-del-nino-jersey.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image26','','imagenes/Head/head-02_04.jpg',1)"><img src="imagenes/Head/head-01_04.jpg" alt="Admisión al Zoológico" name="Image26" width="79" height="43" border="0" id="Image26" /></a></td>
        <td><a href="htmls/visita-parque-jersey.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image27','','imagenes/Head/head-02_05.jpg',1)"><img src="imagenes/Head/head-01_05.jpg" alt="Conoce el Parque" name="Image27" width="79" height="43" border="0" id="Image27" /></a></td>
        <td><a href="htmls/zoologico.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image28','','imagenes/Head/head-02_06.jpg',1)"><img src="imagenes/Head/head-01_06.jpg" alt="Conoce el Zoológico" name="Image28" width="79" height="43" border="0" id="Image28" /></a></td>
        <td><a href="htmls/como-llegar-al-parque-del-nino-jersey.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image29','','imagenes/Head/head-02_07.jpg',1)"><img src="imagenes/Head/head-01_07.jpg" alt="Averigua como llegar al Zoológico" name="Image29" width="79" height="43" border="0" id="Image29" /></a></td>
        <td><a href="htmls/historia-parque-zoologico-jersey.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image30','','imagenes/Head/head-02_08.jpg',1)"><img src="imagenes/Head/head-01_08.jpg" alt="Nuestra historia" name="Image30" width="79" height="43" border="0" id="Image30" /></a></td>
        <td><img src="imagenes/Head/head-01_09.jpg" width="26" height="43" /></td>
      </tr>
      <tr>
        <td colspan="7"><img src="imagenes/Head/head-01_10.jpg" width="497" height="29" border="0" usemap="#Map2" /></td>
      </tr>
    </table></th>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="RegionBanner" --><!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td align="center" valign="top"><table width="800" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th height="35" bgcolor="#FFFFFF" scope="col"><!-- InstanceBeginEditable name="ContenidoCentral" -->
          <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
            <tr bgcolor="#FFFFFF">
              <td><img src="imagenes/Banners/BannerVerano.jpg" alt="Ven y vive un verano refrescante y diviertete a lo grande" width="800" height="298" /></td>
            </tr>
            <tr bgcolor="#FFFFFF">
              <td>&nbsp;</td>
            </tr>
            <tr bgcolor="#FFFFFF">
              <td><table width="800" height="126" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_">
                <tr align="center" valign="top">
                  <td width="13" rowspan="2"><img src="imagenes/images/banners_01.jpg" width="13" height="127" /></td>
                  <td height="96" colspan="2" bgcolor="#FFFFFF"><img src="imagenes/images/banners_02.jpg" alt="Toro Mecánico" width="193" height="96" /></td>
                  <td width="194" height="96" bgcolor="#FFFFFF"><img src="imagenes/images/banners_03.jpg" alt="Tigre en el Zoológico" width="194" height="96" /></td>
                  <td width="193" height="96" bgcolor="#FFFFFF"><img src="imagenes/images/banners_04.jpg" alt="Paseos escolares en el zoológico" width="193" height="96" /></td>
                  <td height="96" colspan="2" bgcolor="#FFFFFF"><img src="imagenes/images/banners_05.jpg" alt="reglamentos de nuestor zoológico" width="194" height="96" /></td>
                  <td width="13" rowspan="2"><img src="imagenes/images/banners-02_01.jpg" width="13" height="127" /></td>
                </tr>
                <tr>
                  <td width="11" height="37" align="left"><img src="imagenes/images/1.jpg" width="11" height="37" /></td>
                  <td width="182" align="center" background="imagenes/images/2.jpg"><a href="htmls/admision-al-parque-del-nino-jersey.php" class="style29">ADMISI&Oacute;N</a></td>
                  <td height="30" align="center" background="imagenes/images/2.jpg"><a href="htmls/zoologico.html" class="style281">ZOOL&Oacute;GICO</a></td>
                  <td height="30" align="center" background="imagenes/images/2.jpg"><a href="htmls/paseos-escolares-zoologico-infantil.html" class="style281">PASEOS ESCOLARES</a></td>
                  <td width="183" height="30" align="center" background="imagenes/images/2.jpg"><a href="htmls/reglamentos.html" class="style281">REGLAMENTOS</a></td>
                  <td width="11" height="37" align="right"><img src="imagenes/images/3.jpg" width="11" height="37" /></td>
                </tr>
              </table></td>
            </tr>
            <tr bgcolor="#FFFFFF">
              <td align="center" valign="top">&nbsp;</td>
            </tr>
            <tr bgcolor="#FFFFFF">
              <td align="center" valign="top"><table width="800" border="0" cellspacing="10">
                <tr>
                  <td bgcolor="#999999"><div align="center"><span class="style31">.:NOTICIAS:.</span></div></td>
                </tr>
              </table></td>
            </tr>
            <tr bgcolor="#FFFFFF">
              <td align="center" valign="top"><table width="800" border="0" cellpadding="0" cellspacing="10">
                <tr>
                  <td width="188" align="left" valign="top"><p align="justify" class="style30">El Zool&oacute;gico Parque del Ni&ntilde;o tiene dos nuevos habitantes, los imponentes Hipop&oacute;tamos.</p>
                    <p align="justify" class="style30"><strong>CARACTER&Iacute;STICAS</strong><br />
                      Tiene un peso entre 2600 y 3500 kg., una longitud   de 5 m., y una altura hasta la cruz de 1'5 m. 
                      Tiene una longevidad de   entre 40 y 50 a&ntilde;os. </p>
                    <p align="justify"><span class="style30">Ven y conocelo!!</span><br />
                    </p></td>
                  <td width="188" align="left" valign="top"><img src="imagenes/images/Hipopotamo.jpg" alt="Nuevo Hipopótamo en el Zoologico Jersey" width="188" height="204" /></td>
                  <td width="187" align="left" valign="top"><p><span class="style30">Ven al parque a conocer a estas hermosas aves</span>, <span class="style30">Los Flamencos.</span></p>
                    <p><span class="style30"><strong>CARACTER&Iacute;STICAS</strong><br />
                      Los flamencos tienen unas delgadas y finas patas, al igual que el cuello, 
                      un tama&ntilde;o de unos 110 cm. 
                      Su envergadura alar est&aacute; en 1 a 1,6 metros. 
                      El peso promedio es de  2.2 kg. </span></p></td>
                  <td width="187" align="left" valign="top"><img src="imagenes/images/flamencos.jpg" alt="" width="187" height="203" /></td>
                </tr>
              </table></td>
            </tr>
          </table>
        <!-- InstanceEndEditable --></th>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top"><map name="MapMap" id="MapMap">
      <area shape="rect" coords="7,11,31,32" href="#" />
      <area shape="rect" coords="38,9,59,35" href="#" />
      <area shape="rect" coords="66,10,87,33" href="#" />
    </map></td>
  </tr>
  <tr>
    <td><table width="800" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th height="70" align="center" valign="middle" scope="col"><table width="418" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="66" scope="col"><img src="imagenes/images/Botones-inferiores-01_04.jpg" width="11" height="50" /></th>
            <th width="66" scope="col"><img src="imagenes/images/Botones-inferiores_01.jpg" width="131" height="50" /></th>
            <th width="11" scope="col"><img src="imagenes/images/Botones-inferiores-01_04.jpg" alt="" width="11" height="50" /></th>
            <th width="132" scope="col"><img src="imagenes/images/Botones-inferiores_03.jpg" alt="Logo Zoologico Jersey, parque del niño" width="131" height="50" /></th>
            <th width="10" scope="col"><img src="imagenes/images/Botones-inferiores-01_04.jpg" alt="" width="11" height="50" /></th>
            <th width="257" scope="col"><a href="http://www.azcarm.com.mx/" target="_blank"><img src="imagenes/images/Botones-inferiores_05.jpg" alt="logo Azcarm - Asociación de Zoológicos y Acuarios de México" width="131" height="50" border="0" /></a></th>
            <th width="257" scope="col"><img src="imagenes/images/Botones-inferiores-01_04.jpg" alt="" width="11" height="50" /></th>
            <th width="257" scope="col"><a href="http://www.google.com/mapmaker?ll=32.086888,-116.598946&amp;spn=0.00749,0.013808&amp;t=h&amp;z=17&amp;q=tijuana+map&amp;gw=30" target="_blank"><img src="imagenes/images/buscanos.jpg" alt="Ver mapa en google" width="131" height="50" border="0" /></a></th>
            <th width="258" scope="col"><img src="imagenes/images/Botones-inferiores-01_04.jpg" alt="" width="11" height="50" /></th>
          </tr>
        </table></th>
      </tr>
      <tr>
        <td><table width="800" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="84" height="43" align="left" valign="top" scope="col"><a href="htmls/contacte-con-el-parque-del-nino-jersey.html"><img src="imagenes/images/contacto.jpg" alt="Contacte con el Zoológico Jersey" width="116" height="43" border="0" /></a></th>
            <th width="600" background="imagenes/images/foot_03.jpg" scope="col">&nbsp;</th>
            <th width="116" height="43" align="right" valign="top" background="imagenes/images/foot_04.jpg" scope="col"><img src="imagenes/images/foot_04.jpg" alt="Zoológico Jersey en Youtube, facebook y Twiteer" width="116" height="43" /></th>
          </tr>
        </table>
          <map name="MapMapMap" id="MapMapMap">
            <area shape="rect" coords="7,11,31,32" href="#" />
            <area shape="rect" coords="38,9,59,35" href="#" />
            <area shape="rect" coords="66,10,87,33" href="#" />
          </map></td>
      </tr>
      <tr>
        <td><img src="imagenes/images/foot_01.jpg" alt="Productos Jersey, yogures, leches, leche agria, choco leche, lacteos" width="800" height="191" /></td>
      </tr>
      <tr>
        <td width="700" align="center" valign="top"><br />
          <div align="center"><span class="style11">Todos los derechos reservados &copy; 2011 | Zoologico Parque del ni&ntilde;o.  | <a href="htmls/contacte-con-el-parque-del-nino-jersey.html">CONTACTO</a><br />
            Delegaci&oacute;n Francisco Zarco, Calle Venustiano Carranza s/n, Valle de Guadalupe,Ensenada B.C</span></div>
          <hr align="center" width="780" /></td>
      </tr>
      <tr>
        <td align="center" valign="middle">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- ImageReady Slices (head.psd) -->
<!-- End ImageReady Slices -->
<map name="Map" id="Map">
  <area shape="rect" coords="7,11,31,32" href="#" />
  <area shape="rect" coords="38,9,59,35" href="#" />
  <area shape="rect" coords="66,10,87,33" href="#" />
</map>
<map name="Map2" id="Map2">
  <area shape="rect" coords="426,3,470,16" href="ingles/home.html" />
</map>
</body>
<!-- InstanceEnd --></html>
